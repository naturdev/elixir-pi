defmodule ApiWeb.OverflowView do
  use ApiWeb, :view
  alias ApiWeb.OverflowView

  def render("index.json", %{overflows: overflows}) do
    %{data: render_many(overflows, OverflowView, "overflow.json")}
  end

  def render("show.json", %{overflow: overflow}) do
    %{data: render_one(overflow, OverflowView, "overflow.json")}
  end

  def render("overflow.json", %{overflow: overflow}) do
    %{id: overflow.id,
      searchstring: overflow.searchstring,
      resultcount: overflow.resultcount,
      resulturl: overflow.resulturl,
      result: overflow.result}
  end
end
