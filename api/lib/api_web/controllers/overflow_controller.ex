defmodule ApiWeb.OverflowController do
  use ApiWeb, :controller

  alias Api.Spike_Context
  alias Api.Spike_Context.Overflow

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    overflows = Spike_Context.list_overflows()
    render(conn, "index.json", overflows: overflows)
  end

  def create(conn, %{"overflow" => overflow_params}) do
    with {:ok, %Overflow{} = overflow} <- Spike_Context.create_overflow(overflow_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", overflow_path(conn, :show, overflow))
      |> render("show.json", overflow: overflow)
    end
  end

  def show(conn, %{"id" => id}) do
    overflow = Spike_Context.get_overflow!(id)
    render(conn, "show.json", overflow: overflow)
  end

  def update(conn, %{"id" => id, "overflow" => overflow_params}) do
    overflow = Spike_Context.get_overflow!(id)

    with {:ok, %Overflow{} = overflow} <- Spike_Context.update_overflow(overflow, overflow_params) do
      render(conn, "show.json", overflow: overflow)
    end
  end

  def delete(conn, %{"id" => id}) do
    overflow = Spike_Context.get_overflow!(id)
    with {:ok, %Overflow{}} <- Spike_Context.delete_overflow(overflow) do
      send_resp(conn, :no_content, "")
    end
  end
end
