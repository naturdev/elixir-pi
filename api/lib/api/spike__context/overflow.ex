defmodule Api.Spike_Context.Overflow do
  use Ecto.Schema
  import Ecto.Changeset


  schema "overflows" do
    field :result, :string
    field :resultcount, :integer
    field :resulturl, :string
    field :searchstring, :string

    timestamps()
  end

  @doc false
  def changeset(overflow, attrs) do
    overflow
    |> cast(attrs, [:searchstring, :resultcount, :resulturl, :result])
    |> validate_required([:searchstring, :resultcount, :resulturl, :result])
  end
end
