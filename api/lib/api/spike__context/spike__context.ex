defmodule Api.Spike_Context do
  @moduledoc """
  The Spike_Context context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Spike_Context.Overflow

  @doc """
  Returns the list of overflows.

  ## Examples

      iex> list_overflows()
      [%Overflow{}, ...]

  """
  def list_overflows do
    Repo.all(Overflow)
  end

  @doc """
  Gets a single overflow.

  Raises `Ecto.NoResultsError` if the Overflow does not exist.

  ## Examples

      iex> get_overflow!(123)
      %Overflow{}

      iex> get_overflow!(456)
      ** (Ecto.NoResultsError)

  """
  def get_overflow!(id), do: Repo.get!(Overflow, id)

  @doc """
  Creates a overflow.

  ## Examples

      iex> create_overflow(%{field: value})
      {:ok, %Overflow{}}

      iex> create_overflow(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_overflow(attrs \\ %{}) do
    %Overflow{}
    |> Overflow.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a overflow.

  ## Examples

      iex> update_overflow(overflow, %{field: new_value})
      {:ok, %Overflow{}}

      iex> update_overflow(overflow, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_overflow(%Overflow{} = overflow, attrs) do
    overflow
    |> Overflow.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Overflow.

  ## Examples

      iex> delete_overflow(overflow)
      {:ok, %Overflow{}}

      iex> delete_overflow(overflow)
      {:error, %Ecto.Changeset{}}

  """
  def delete_overflow(%Overflow{} = overflow) do
    Repo.delete(overflow)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking overflow changes.

  ## Examples

      iex> change_overflow(overflow)
      %Ecto.Changeset{source: %Overflow{}}

  """
  def change_overflow(%Overflow{} = overflow) do
    Overflow.changeset(overflow, %{})
  end
end
