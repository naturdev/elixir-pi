defmodule Api.Repo.Migrations.CreateOverflows do
  use Ecto.Migration

  def change do
    create table(:overflows) do
      add :searchstring, :string
      add :resultcount, :integer
      add :resulturl, :string
      add :result, :text

      timestamps()
    end

  end
end
