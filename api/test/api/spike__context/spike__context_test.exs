defmodule Api.Spike_ContextTest do
  use Api.DataCase

  alias Api.Spike_Context

  describe "overflows" do
    alias Api.Spike_Context.Overflow

    @valid_attrs %{result: "some result", resultcount: 42, resulturl: "some resulturl", searchstring: "some searchstring"}
    @update_attrs %{result: "some updated result", resultcount: 43, resulturl: "some updated resulturl", searchstring: "some updated searchstring"}
    @invalid_attrs %{result: nil, resultcount: nil, resulturl: nil, searchstring: nil}

    def overflow_fixture(attrs \\ %{}) do
      {:ok, overflow} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Spike_Context.create_overflow()

      overflow
    end

    test "list_overflows/0 returns all overflows" do
      overflow = overflow_fixture()
      assert Spike_Context.list_overflows() == [overflow]
    end

    test "get_overflow!/1 returns the overflow with given id" do
      overflow = overflow_fixture()
      assert Spike_Context.get_overflow!(overflow.id) == overflow
    end

    test "create_overflow/1 with valid data creates a overflow" do
      assert {:ok, %Overflow{} = overflow} = Spike_Context.create_overflow(@valid_attrs)
      assert overflow.result == "some result"
      assert overflow.resultcount == 42
      assert overflow.resulturl == "some resulturl"
      assert overflow.searchstring == "some searchstring"
    end

    test "create_overflow/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Spike_Context.create_overflow(@invalid_attrs)
    end

    test "update_overflow/2 with valid data updates the overflow" do
      overflow = overflow_fixture()
      assert {:ok, overflow} = Spike_Context.update_overflow(overflow, @update_attrs)
      assert %Overflow{} = overflow
      assert overflow.result == "some updated result"
      assert overflow.resultcount == 43
      assert overflow.resulturl == "some updated resulturl"
      assert overflow.searchstring == "some updated searchstring"
    end

    test "update_overflow/2 with invalid data returns error changeset" do
      overflow = overflow_fixture()
      assert {:error, %Ecto.Changeset{}} = Spike_Context.update_overflow(overflow, @invalid_attrs)
      assert overflow == Spike_Context.get_overflow!(overflow.id)
    end

    test "delete_overflow/1 deletes the overflow" do
      overflow = overflow_fixture()
      assert {:ok, %Overflow{}} = Spike_Context.delete_overflow(overflow)
      assert_raise Ecto.NoResultsError, fn -> Spike_Context.get_overflow!(overflow.id) end
    end

    test "change_overflow/1 returns a overflow changeset" do
      overflow = overflow_fixture()
      assert %Ecto.Changeset{} = Spike_Context.change_overflow(overflow)
    end
  end
end
