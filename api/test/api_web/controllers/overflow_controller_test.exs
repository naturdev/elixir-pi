defmodule ApiWeb.OverflowControllerTest do
  use ApiWeb.ConnCase

  alias Api.Spike_Context
  alias Api.Spike_Context.Overflow

  @create_attrs %{result: "some result", resultcount: 42, resulturl: "some resulturl", searchstring: "some searchstring"}
  @update_attrs %{result: "some updated result", resultcount: 43, resulturl: "some updated resulturl", searchstring: "some updated searchstring"}
  @invalid_attrs %{result: nil, resultcount: nil, resulturl: nil, searchstring: nil}

  def fixture(:overflow) do
    {:ok, overflow} = Spike_Context.create_overflow(@create_attrs)
    overflow
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all overflows", %{conn: conn} do
      conn = get conn, overflow_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create overflow" do
    test "renders overflow when data is valid", %{conn: conn} do
      conn = post conn, overflow_path(conn, :create), overflow: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, overflow_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "result" => "some result",
        "resultcount" => 42,
        "resulturl" => "some resulturl",
        "searchstring" => "some searchstring"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, overflow_path(conn, :create), overflow: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update overflow" do
    setup [:create_overflow]

    test "renders overflow when data is valid", %{conn: conn, overflow: %Overflow{id: id} = overflow} do
      conn = put conn, overflow_path(conn, :update, overflow), overflow: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, overflow_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "result" => "some updated result",
        "resultcount" => 43,
        "resulturl" => "some updated resulturl",
        "searchstring" => "some updated searchstring"}
    end

    test "renders errors when data is invalid", %{conn: conn, overflow: overflow} do
      conn = put conn, overflow_path(conn, :update, overflow), overflow: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete overflow" do
    setup [:create_overflow]

    test "deletes chosen overflow", %{conn: conn, overflow: overflow} do
      conn = delete conn, overflow_path(conn, :delete, overflow)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, overflow_path(conn, :show, overflow)
      end
    end
  end

  defp create_overflow(_) do
    overflow = fixture(:overflow)
    {:ok, overflow: overflow}
  end
end
